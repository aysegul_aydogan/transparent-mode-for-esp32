## Transparent Mode

Firstly, you should download VScode ESP-IDF Extension. 

After you Build > Flash > Monitor, code expects the message from the serial COM Port.

Download the "Serial Comm Tester" terminal to send the data over UART0. 

In the code, by changing the macro definition  "DATA_TYPE" , you can take data
such as string and hex.

Your data will be parse and it will be considered according to our data transmission 
protocol.

## Protocol Details

* First 2 Bytes represents the header and header must be "DE DE".
* Next 3 Bytes represents the peripheral type. It must be entered as ascii response.
  For example, for SPI = 53 50 49 
               for I2C = 49 32 4D

* Next 1 Byte respresents the channel of peripheral
  For example, for SPI0 = 53 50 49 30
  Note: Consider channel as char and send the ascii response of it. 

* Next 2 Bytes represents the lenght of payload data. 

* Last 2 Byte represents the suffix which is end of the data.

* Bytes, which are between suffix and length, holds the payload. 






