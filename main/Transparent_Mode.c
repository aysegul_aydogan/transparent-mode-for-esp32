/****************************************
 * Autor: Aysegul Aydogan
 * Start date: 26.01.2022
 * Last modified: 02.02.2022
 * Written purpose: Transparent mode for ESP32-S
 * Quick testing for peripherals
******************************************/

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/uart.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
/*#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE*/
#include "esp_log.h"
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
 
#define ECHO_TEST_TXD (CONFIG_EXAMPLE_UART_TXD)
#define ECHO_TEST_RXD (CONFIG_EXAMPLE_UART_RXD)
#define ECHO_TEST_RTS (UART_PIN_NO_CHANGE)
#define ECHO_TEST_CTS (UART_PIN_NO_CHANGE)
#define ECHO_UART_PORT_NUM      (CONFIG_EXAMPLE_UART_PORT_NUM)
#define ECHO_UART_BAUD_RATE     (CONFIG_EXAMPLE_UART_BAUD_RATE)
#define ECHO_TASK_STACK_SIZE    (CONFIG_EXAMPLE_TASK_STACK_SIZE)
/* MAX DATA LENGTH CAN BE FFFF */
#define BUF_SIZE (32768)


/* 0 - RECEIVED DATA IS STRING */
/* 1 - RECEIVED DATA IS HEX */
#define DATA_TYPE 1 

void* received = NULL;
uint32_t peripheral;
uint8_t channel;
uint16_t data_length;
char data[BUF_SIZE];

typedef enum
{
    Success,
    FormatError,
    HeaderError,
    DataLengthError,
    UndefinedPeripheral,
    SuffixError
}ReturnValues;

typedef enum
{
    SPI = 0x535049,
    I2C = 0x49324D,
    URT = 0x555254,
    ADC = 0x414443,
    DAC = 0x444143,
    WFI = 0x574649,
    BLE = 0x424C45,
}Peripherals;

typedef struct {
    uint32_t peripheral;
    uint8_t channel;
    uint16_t data_length;
    char data[BUF_SIZE];
} message;

/* HELPER FUNCTIONS */
unsigned int str2hex(char *str, int start, int end)
{
    unsigned int hex = 0;
    for (int i = start; i < strlen(str) && i < end; i++)
    {
        if (str[i] >= '0' && str[i] <= '9')
        {
            hex = hex * 16 + str[i] - '0';
        }
        else if (str[i] >= 'A' && str[i] <= 'F')
        {
            hex = hex * 16 + str[i] - 'A' + 10;
        }
        else if (str[i] >= 'a' && str[i] <= 'f')
        {
            hex = hex * 16 + str[i] - 'a' + 10;
        }
        /*IF STR INCLUDES NON-HEX VALUE, RETURN 0 */
        else
        {
            return 0;
        }
    }
    return hex;
}

char * concatHex2String(uint8_t *arr, int size) 
{
    char *str = (char*) malloc(size * 2 + 1);
    int i;
    for (i = 0; i < size; i++)
    {
        sprintf(str + i * 2, "%02x", arr[i]);
    }
    str[size * 2] = '\0';
    return str;
}


/* PARSING THE RECEIVED DATA */
unsigned int parseMessage(char* received, uint32_t* peripheral, uint8_t* channel, uint16_t* data_length, char* data) {

    /* CONVERT TO UPPERCASE */
    for (int i = 0; i < strlen(received); i++) {
        received[i] = toupper(received[i]);
    }

    /* CHECK FOR FORMAT ALL RECEIVED DATA */
    if (strlen(received) < 14 || str2hex(received, 0, strlen(received)) == 0) {
        return FormatError;
    }

    /* CHECK FOR HEADER */
    if (received[0] != 'D' || received[1] != 'E' || received[2] != 'D' || received[3] != 'E') {
        return HeaderError;
    }

    /* CHECK FOR SUFFIX */
    if (received[strlen(received) - 2] != 'D' && received[strlen(received) - 1] != 'A') {
        return SuffixError;
    }
    
    int suffix_index = strlen(received) - 2;

    /* CHECK FOR DEFINED PERIPHERAL */
    *peripheral = str2hex(received, 4, 10);
    if (*peripheral != SPI && *peripheral != I2C && *peripheral != URT && *peripheral != ADC && *peripheral != DAC && *peripheral != WFI && *peripheral != BLE) {
        return UndefinedPeripheral;
    }

    /* TAKE THE CHANNEL INFO */
    *channel = str2hex(received, 10, 12);

    /* CHECK FOR DATA LENGTH -2 BYTES- */
    *data_length = str2hex(received, 12, 16);
    *data_length *= 2;
    
    if (*data_length > BUF_SIZE || *data_length == 0 || *data_length > suffix_index - 16) {
        return DataLengthError;
    }

    /* TAKE THE DATA */
    /* ONE BYTE IS TWO HEX VALUES */
    for (int i = 0; i < *data_length; i++) {
        data[i] = received[16 + i];
    }

    *data_length /= 2;

    return Success;
}

/* ASSIGN THE RECEIVED DATA TO THE CORRECT VARIABLE OF STRUCT */
message* createStruct(uint32_t* peripheral, uint8_t* channel, uint16_t* data_length, char* data) {
    message* msg = malloc(sizeof(message));
    msg->peripheral = *peripheral;
    msg->channel = *channel;
    msg->data_length = *data_length;
    strcpy(msg->data, data);
    return msg;
}

static void directData(void *arg)
{
    void* received = malloc(BUF_SIZE);
    /* CONFIGURE PARAMETERS OF AN UART DRIVER */
    /* COMMUNICATION PINS AND INSTALL THE DRIVER */
    uart_config_t uart_config = {
        .baud_rate = ECHO_UART_BAUD_RATE,
        .data_bits = UART_DATA_8_BITS,
        .parity    = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_APB,
    };

    int intr_alloc_flags = 0;
    #if CONFIG_UART_ISR_IN_IRAM
        intr_alloc_flags = ESP_INTR_FLAG_IRAM;
    #endif

    //UART0 
    #define UART0_PORT_NUM 0
    #define CONFIG_UART0_RXD 3 
    #define CONFIG_UART0_TXD 1
    ESP_ERROR_CHECK(uart_driver_install(UART0_PORT_NUM, BUF_SIZE * 2, 0, 0, NULL, intr_alloc_flags));
    ESP_ERROR_CHECK(uart_param_config(UART0_PORT_NUM, &uart_config));
    ESP_ERROR_CHECK(uart_set_pin(UART0_PORT_NUM, CONFIG_UART0_TXD, CONFIG_UART0_RXD, ECHO_TEST_RTS, ECHO_TEST_CTS));

    ESP_LOGI("\nDIRECT DATA", "Begin");

    while (1) 
    {
        /* READ THE DATA FROM THE UART */   
        int len0 = uart_read_bytes(UART0_PORT_NUM, received, BUF_SIZE, 20 / portTICK_RATE_MS);
        if(len0 > 0)
        {
            char* input_str = (char*) received;
            input_str[len0] = '\0';

            /* IF THE RECEIVED DATA IS HEX */
            if (DATA_TYPE == 1)
            {
                uint8_t* input_arr = (uint8_t*)received;

                input_str = concatHex2String(input_arr, len0);

            }

            //ESP_LOGI("\nDIRECT DATA", "RECEIVED %s :\n", (char*)received);
            
            /*IF THE RECEIVED DATA IS VALID AND CAN BE PARSED */
            uint32_t return_value = parseMessage(input_str, &peripheral, &channel, &data_length, data);

            if (return_value == Success) 
            {
                message* msg = createStruct(&peripheral, &channel, &data_length, data);
               
                ESP_LOGI("\nDIRECT DATA\n", "PERIPHERAL: %x\nCHANNEL: %c\nDATA LENGTH: %d BYTE\nDATA: %s\n", msg->peripheral, msg->channel, msg->data_length, msg->data);
            
                switch (peripheral) {
                    case SPI:
                        if(msg->channel != '0' && msg->channel != '1' && msg->channel != '2' && msg->channel != '3')
                        {
                            ESP_LOGI("\nDIRECT DATA", "CHANNEL ERROR");
                            break;
                        }
                        break;
                    case I2C:
                        printf("I2C\n");
                        break;
                    case URT:
                        printf("URT\n");
                        break;
                    case ADC:
                        printf("ADC\n");
                        break;
                    case DAC:
                        printf("DAC\n");
                        break;
                    case WFI:
                        printf("WFI\n");
                        break;
                    case BLE:
                        printf("BLE\n");
                        break;
                    default:
                        break;
                }
            }
            else 
            {
                printf("Error: %d\n", return_value);
            }
        }
    }
        vTaskDelay(1 / portTICK_RATE_MS);
}

void app_main(void)
{
    
    xTaskCreate(directData, "Transparent Mode", ECHO_TASK_STACK_SIZE, NULL, 10, NULL);   
}